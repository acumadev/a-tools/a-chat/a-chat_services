defmodule AChatServices.A_ChatTest do
  use AChatServices.DataCase

  alias AChatServices.A_Chat

  describe "servers" do
    alias AChatServices.A_Chat.Server

    @valid_attrs %{description: "some description", logo: "some logo", name: "some name", rank: "some rank"}
    @update_attrs %{description: "some updated description", logo: "some updated logo", name: "some updated name", rank: "some updated rank"}
    @invalid_attrs %{description: nil, logo: nil, name: nil, rank: nil}

    def server_fixture(attrs \\ %{}) do
      {:ok, server} =
        attrs
        |> Enum.into(@valid_attrs)
        |> A_Chat.create_server()

      server
    end

    test "list_servers/0 returns all servers" do
      server = server_fixture()
      assert A_Chat.list_servers() == [server]
    end

    test "get_server!/1 returns the server with given id" do
      server = server_fixture()
      assert A_Chat.get_server!(server.id) == server
    end

    test "create_server/1 with valid data creates a server" do
      assert {:ok, %Server{} = server} = A_Chat.create_server(@valid_attrs)
      assert server.description == "some description"
      assert server.logo == "some logo"
      assert server.name == "some name"
      assert server.rank == "some rank"
    end

    test "create_server/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = A_Chat.create_server(@invalid_attrs)
    end

    test "update_server/2 with valid data updates the server" do
      server = server_fixture()
      assert {:ok, %Server{} = server} = A_Chat.update_server(server, @update_attrs)
      assert server.description == "some updated description"
      assert server.logo == "some updated logo"
      assert server.name == "some updated name"
      assert server.rank == "some updated rank"
    end

    test "update_server/2 with invalid data returns error changeset" do
      server = server_fixture()
      assert {:error, %Ecto.Changeset{}} = A_Chat.update_server(server, @invalid_attrs)
      assert server == A_Chat.get_server!(server.id)
    end

    test "delete_server/1 deletes the server" do
      server = server_fixture()
      assert {:ok, %Server{}} = A_Chat.delete_server(server)
      assert_raise Ecto.NoResultsError, fn -> A_Chat.get_server!(server.id) end
    end

    test "change_server/1 returns a server changeset" do
      server = server_fixture()
      assert %Ecto.Changeset{} = A_Chat.change_server(server)
    end
  end
end
