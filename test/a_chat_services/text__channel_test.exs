defmodule AChatServices.Text_ChannelTest do
  use AChatServices.DataCase

  alias AChatServices.Text_Channel

  describe "messages" do
    alias AChatServices.Text_Channel.Message

    @valid_attrs %{message: "some message", user_id: 42, username: "some username"}
    @update_attrs %{message: "some updated message", user_id: 43, username: "some updated username"}
    @invalid_attrs %{message: nil, user_id: nil, username: nil}

    def message_fixture(attrs \\ %{}) do
      {:ok, message} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Text_Channel.create_message()

      message
    end

    test "list_messages/0 returns all messages" do
      message = message_fixture()
      assert Text_Channel.list_messages() == [message]
    end

    test "get_message!/1 returns the message with given id" do
      message = message_fixture()
      assert Text_Channel.get_message!(message.id) == message
    end

    test "create_message/1 with valid data creates a message" do
      assert {:ok, %Message{} = message} = Text_Channel.create_message(@valid_attrs)
      assert message.message == "some message"
      assert message.user_id == 42
      assert message.username == "some username"
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Text_Channel.create_message(@invalid_attrs)
    end

    test "update_message/2 with valid data updates the message" do
      message = message_fixture()
      assert {:ok, %Message{} = message} = Text_Channel.update_message(message, @update_attrs)
      assert message.message == "some updated message"
      assert message.user_id == 43
      assert message.username == "some updated username"
    end

    test "update_message/2 with invalid data returns error changeset" do
      message = message_fixture()
      assert {:error, %Ecto.Changeset{}} = Text_Channel.update_message(message, @invalid_attrs)
      assert message == Text_Channel.get_message!(message.id)
    end

    test "delete_message/1 deletes the message" do
      message = message_fixture()
      assert {:ok, %Message{}} = Text_Channel.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Text_Channel.get_message!(message.id) end
    end

    test "change_message/1 returns a message changeset" do
      message = message_fixture()
      assert %Ecto.Changeset{} = Text_Channel.change_message(message)
    end
  end

  describe "commands" do
    alias AChatServices.Text_Channel.Command

    @valid_attrs %{category: "some category", command: "some command", description: "some description", enabled: true, response: %{}}
    @update_attrs %{category: "some updated category", command: "some updated command", description: "some updated description", enabled: false, response: %{}}
    @invalid_attrs %{category: nil, command: nil, description: nil, enabled: nil, response: nil}

    def command_fixture(attrs \\ %{}) do
      {:ok, command} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Text_Channel.create_command()

      command
    end

    test "list_commands/0 returns all commands" do
      command = command_fixture()
      assert Text_Channel.list_commands() == [command]
    end

    test "get_command!/1 returns the command with given id" do
      command = command_fixture()
      assert Text_Channel.get_command!(command.id) == command
    end

    test "create_command/1 with valid data creates a command" do
      assert {:ok, %Command{} = command} = Text_Channel.create_command(@valid_attrs)
      assert command.category == "some category"
      assert command.command == "some command"
      assert command.description == "some description"
      assert command.enabled == true
      assert command.response == %{}
    end

    test "create_command/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Text_Channel.create_command(@invalid_attrs)
    end

    test "update_command/2 with valid data updates the command" do
      command = command_fixture()
      assert {:ok, %Command{} = command} = Text_Channel.update_command(command, @update_attrs)
      assert command.category == "some updated category"
      assert command.command == "some updated command"
      assert command.description == "some updated description"
      assert command.enabled == false
      assert command.response == %{}
    end

    test "update_command/2 with invalid data returns error changeset" do
      command = command_fixture()
      assert {:error, %Ecto.Changeset{}} = Text_Channel.update_command(command, @invalid_attrs)
      assert command == Text_Channel.get_command!(command.id)
    end

    test "delete_command/1 deletes the command" do
      command = command_fixture()
      assert {:ok, %Command{}} = Text_Channel.delete_command(command)
      assert_raise Ecto.NoResultsError, fn -> Text_Channel.get_command!(command.id) end
    end

    test "change_command/1 returns a command changeset" do
      command = command_fixture()
      assert %Ecto.Changeset{} = Text_Channel.change_command(command)
    end
  end
end
