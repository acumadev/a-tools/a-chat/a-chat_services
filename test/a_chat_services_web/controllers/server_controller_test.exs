defmodule AChatServicesWeb.ServerControllerTest do
  use AChatServicesWeb.ConnCase

  alias AChatServices.A_Chat
  alias AChatServices.A_Chat.Server

  @create_attrs %{
    description: "some description",
    logo: "some logo",
    name: "some name",
    rank: "some rank"
  }
  @update_attrs %{
    description: "some updated description",
    logo: "some updated logo",
    name: "some updated name",
    rank: "some updated rank"
  }
  @invalid_attrs %{description: nil, logo: nil, name: nil, rank: nil}

  def fixture(:server) do
    {:ok, server} = A_Chat.create_server(@create_attrs)
    server
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all servers", %{conn: conn} do
      conn = get(conn, Routes.server_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create server" do
    test "renders server when data is valid", %{conn: conn} do
      conn = post(conn, Routes.server_path(conn, :create), server: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.server_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some description",
               "logo" => "some logo",
               "name" => "some name",
               "rank" => "some rank"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.server_path(conn, :create), server: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update server" do
    setup [:create_server]

    test "renders server when data is valid", %{conn: conn, server: %Server{id: id} = server} do
      conn = put(conn, Routes.server_path(conn, :update, server), server: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.server_path(conn, :show, id))

      assert %{
               "id" => id,
               "description" => "some updated description",
               "logo" => "some updated logo",
               "name" => "some updated name",
               "rank" => "some updated rank"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, server: server} do
      conn = put(conn, Routes.server_path(conn, :update, server), server: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete server" do
    setup [:create_server]

    test "deletes chosen server", %{conn: conn, server: server} do
      conn = delete(conn, Routes.server_path(conn, :delete, server))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.server_path(conn, :show, server))
      end
    end
  end

  defp create_server(_) do
    server = fixture(:server)
    {:ok, server: server}
  end
end
