defmodule AChatServices.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :message, :string
      add :username, :string
      add :user_id, :integer
      add :edited, :boolean, default: false, null: false
      add :deleted, :boolean, default: false, null: false
      
      timestamps()
    end

  end
end
