defmodule AChatServices.Repo.Migrations.CreateCommands do
  use Ecto.Migration

  def change do
    create table(:commands) do
      add :command, :string
      add :description, :string
      add :response, :map
      add :enabled, :boolean, default: true, null: false
      add :category, :string

      timestamps()
    end

  end
end
