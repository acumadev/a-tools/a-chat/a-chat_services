defmodule AChatServices.Repo.Migrations.CreateServers do
  use Ecto.Migration

  def change do
    create table(:servers) do
      add :name, :string
      add :description, :string
      add :logo, :text
      add :rank, :string

      timestamps()
    end

  end
end
