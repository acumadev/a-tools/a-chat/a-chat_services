defmodule AChatServicesWeb.UserSocket do
  use Phoenix.Socket

  ## Channels
  # channel "room:*", AChatServicesWeb.RoomChannel
  channel "text:*", AChatServicesWeb.TextChannel
  
  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.
  def connect(%{"credentials" => credentials}, socket, _connect_info) do
    body = Poison.encode! %{"credentials" => credentials}
    resp = HTTPoison.post! "http://localhost:4001/api/login", body, [{"Content-Type", "application/json"}]
    body = Poison.decode! resp.body
    {:ok, assign(socket, :user, body["data"])}
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     AChatServicesWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(_socket), do: nil
end
