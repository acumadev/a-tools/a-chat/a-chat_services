defmodule AChatServicesWeb.TextChannel do
  use AChatServicesWeb, :channel
  
  alias AChatServices.Text_Channel

  def join("text:"<>channel_name, payload, socket) do
    if authorized?(payload) do
      send(self(), :after_join)
      IO.inspect(channel_name)
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("message:new", payload, socket) do
    IO.inspect(socket.assigns.user)
    payload = payload
    |> Map.put("user_id", socket.assigns.user["id"])
    |> Map.put("username", socket.assigns.user["username"])
    with {:ok, message} <- Text_Channel.create_message(payload) do
      payload = payload 
      |> Map.put("id", message.id)
      broadcast socket, "message:new", payload
      String.split(payload["message"]," ")
      |> Enum.map(&command_interceptor(&1, socket))
      {:noreply, socket}
    end
  end 

  def handle_in("message:edit", payload, socket) do
    message = Text_Channel.get_message!(payload["id"])
    payload = payload
    |> Map.put("edited", true)
    with {:ok, _ } <- Text_Channel.update_message(message, payload) do
      broadcast socket, "message:edit", payload
      {:noreply, socket}
    end
  end

  def handle_in("message:delete", payload, socket) do
    message = Text_Channel.get_message!(payload["id"])
    with {:ok, _ } <- Text_Channel.update_message(message, %{deleted: true}) do
      broadcast socket, "message:delete", payload
      {:noreply, socket}
    end
  end

  def handle_info(:after_join, socket) do
    Text_Channel.list_newest_messages()
    |> Enum.each(fn msg -> push(socket, "message:new", %{
        username: socket.assigns.user["username"],
        message: msg.message,
        user_id: socket.assigns.user["id"],
        deleted: msg.deleted,
        id: msg.id
      }) end)
    {:noreply, socket} # :noreply
  end
  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

  defp command_interceptor(word, socket) do
    command = Text_Channel.get_command_by!(word)  
    if command do 
      with {:ok, message} <- Text_Channel.create_message(%{username: "A-bot", message: command.response["answer"], user_id: -1}) do
        broadcast socket, "command:answer", %{username: message.username, message: message.message}
        {:noreply, socket}
      end
    end  
  end
end
