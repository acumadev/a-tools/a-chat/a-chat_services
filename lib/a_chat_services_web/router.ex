defmodule AChatServicesWeb.Router do
  use AChatServicesWeb, :router

  pipeline :api do
    plug CORSPlug, origin: ["http://localhost:4200"]
    plug :accepts, ["json"]
  end

  scope "/api", AChatServicesWeb do
    pipe_through :api

    resources "/servers", ServerController, except: [:new, :edit]
    options   "/servers", ServerController, :options

    resources "/commands", CommandController, except: [:new, :edit]
    options   "/commands", CommandController, :options
  end
end
