defmodule AChatServicesWeb.ServerController do
  use AChatServicesWeb, :controller

  alias AChatServices.A_Chat
  alias AChatServices.A_Chat.Server

  action_fallback AChatServicesWeb.FallbackController

  def index(conn, _params) do
    servers = A_Chat.list_servers()
    render(conn, "index.json", servers: servers)
  end

  def create(conn, %{"server" => server_params}) do
    with {:ok, %Server{} = server} <- A_Chat.create_server(server_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.server_path(conn, :show, server))
      |> render("show.json", server: server)
    end
  end

  def show(conn, %{"id" => id}) do
    server = A_Chat.get_server!(id)
    render(conn, "show.json", server: server)
  end

  def update(conn, %{"id" => id, "server" => server_params}) do
    server = A_Chat.get_server!(id)

    with {:ok, %Server{} = server} <- A_Chat.update_server(server, server_params) do
      render(conn, "show.json", server: server)
    end
  end

  def delete(conn, %{"id" => id}) do
    server = A_Chat.get_server!(id)

    with {:ok, %Server{}} <- A_Chat.delete_server(server) do
      send_resp(conn, :no_content, "")
    end
  end
end
