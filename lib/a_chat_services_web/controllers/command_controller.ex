defmodule AChatServicesWeb.CommandController do
  use AChatServicesWeb, :controller

  alias AChatServices.Text_Channel
  alias AChatServices.Text_Channel.Command

  action_fallback AChatServicesWeb.FallbackController

  def index(conn, _params) do
    commands = Text_Channel.list_commands()
    render(conn, "index.json", commands: commands)
  end

  def create(conn, %{"command" => command_params}) do
    with {:ok, %Command{} = command} <- Text_Channel.create_command(command_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.command_path(conn, :show, command))
      |> render("show.json", command: command)
    end
  end

  def show(conn, %{"id" => id}) do
    command = Text_Channel.get_command!(id)
    render(conn, "show.json", command: command)
  end

  def update(conn, %{"id" => id, "command" => command_params}) do
    command = Text_Channel.get_command!(id)

    with {:ok, %Command{} = command} <- Text_Channel.update_command(command, command_params) do
      render(conn, "show.json", command: command)
    end
  end

  def delete(conn, %{"id" => id}) do
    command = Text_Channel.get_command!(id)

    with {:ok, %Command{}} <- Text_Channel.delete_command(command) do
      send_resp(conn, :no_content, "")
    end
  end
end
