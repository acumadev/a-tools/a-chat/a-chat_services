defmodule AChatServicesWeb.CommandView do
  use AChatServicesWeb, :view
  alias AChatServicesWeb.CommandView

  def render("index.json", %{commands: commands}) do
    %{data: render_many(commands, CommandView, "command.json")}
  end

  def render("show.json", %{command: command}) do
    %{data: render_one(command, CommandView, "command.json")}
  end

  def render("command.json", %{command: command}) do
    %{id: command.id,
      command: command.command,
      description: command.description,
      response: command.response,
      enabled: command.enabled,
      category: command.category}
  end
end
