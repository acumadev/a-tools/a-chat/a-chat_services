defmodule AChatServices.A_Chat.Server do
  use Ecto.Schema
  import Ecto.Changeset

  schema "servers" do
    field :description, :string
    field :logo, :string
    field :name, :string
    field :rank, :string

    timestamps()
  end

  @doc false
  def changeset(server, attrs) do
    server
    |> cast(attrs, [:name, :description, :logo, :rank])
    |> validate_required([:name, :description, :logo, :rank])
  end
end
