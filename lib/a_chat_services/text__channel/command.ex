defmodule AChatServices.Text_Channel.Command do
  use Ecto.Schema
  import Ecto.Changeset

  schema "commands" do
    field :category, :string
    field :command, :string
    field :description, :string
    field :enabled, :boolean, default: true
    field :response, :map

    timestamps()
  end

  @doc false
  def changeset(command, attrs) do
    command
    |> cast(attrs, [:command, :description, :response, :enabled, :category])
    |> validate_required([:command, :description, :response, :enabled, :category])
  end
end
