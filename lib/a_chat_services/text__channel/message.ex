defmodule AChatServices.Text_Channel.Message do
  use Ecto.Schema
  import Ecto.Changeset

  schema "messages" do
    field :message, :string
    field :user_id, :integer
    field :username, :string
    field :edited, :boolean, default: false
    field :deleted, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:message, :username, :user_id, :edited, :deleted])
    |> validate_required([:message, :username, :user_id, :edited, :deleted])
  end
end
