defmodule AChatServices.Repo do
  use Ecto.Repo,
    otp_app: :a_chat_services,
    adapter: Ecto.Adapters.Postgres
end
